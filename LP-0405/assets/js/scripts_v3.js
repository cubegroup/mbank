$(document).ready(function () {

    var attr = $(location).attr('search').split('&').map(el=>(el.split('=')[1]));

    // woman
    if (attr[0] === '2') {
        $('.bg-header').addClass('bg-header-woman');
        $('.bg-section-1').addClass('bg-section-1-woman');
        $('.bg-section-2').addClass('bg-section-2-woman');
    } else {
        $('.bg-header').addClass('bg-header-man');
        $('.bg-section-1').addClass('bg-section-1-man');
        $('.bg-section-2').addClass('bg-section-2-man');
    }

    $('[data-toggle="tooltip"]').tooltip();

    $(".carousel-inner").swipe( {
        swipeLeft:function() {
            $(this).parent().carousel('next');
        },
        swipeRight:function() {
            $(this).parent().carousel('prev');
        },
    });

    var modal = $('#myModal');
    var modalBackground = document.getElementById("myModal");
    var form = document.querySelector("form#contact");
    var $form = $("form#contact");
    var $hash = $form.find('[name=hash]');
    var loadPage = '';
    var sendForm = '';

    function generateHash() {
        $form.addClass('disabled');
        var uniqueId = Math.random().toString(36).substring(2) + (new Date()).getTime().toString(36);
        $hash.val(uniqueId);
        loadPage = uniqueId;
        sendForm = uniqueId;
        $('body').append('<img src="https://tbl.tradedoubler.com/report?organization=2050824&event=365730&leadNumber=' + loadPage + '">');
        $form.removeClass('disabled');
    }

    generateHash();

    var constraints = {
        name: {
            presence: {
                message: "^To pole jest wymagane"
            }
        },
        email: {
            presence: {
                message: "^To pole jest wymagane"
            },
            email: {
                message: "^Wprowadź poprawny adres e-mail"
            }
        },
        phone: {
            presence: {
                message: "^To pole jest wymagane"
            },
            format: {
                pattern: "\\d{9}",
                message: "^Wprowadź prawidłowy numer telefonu (9 cyfr, tylko polskie numery)"
            }
        },
        nip: {
            presence: {
                message: "^To pole jest wymagane"
            },
            format: {
                pattern: "\\d{10}",
                message: "^Wprowadź prawidłowy nr NIP (10 cyfr)"
            },
            nipPL: true
        },
        postal_code: {
            presence: {
                message: "^To pole jest wymagane"
            },
            format: {
                pattern: "\\d{2}-\\d{3}",
                message: "^Wprowadź prawidłowy kod pocztowy (2 cyfry, myślnik, 3 cyfry)"
            }
        },
        rules1: {
            presence: {
                message: "^To pole jest wymagane"
            },
            inclusion: {
                within: [true],
                message: "^To pole jest wymagane"
            }
        },
        rules2: {
            presence: {
                message: "^To pole jest wymagane"
            },
            inclusion: {
                within: [true],
                message: "^To pole jest wymagane"
            }
        }
    };

    validate.validators.nipPL = function (value, options, key, attributes) {
        if (value != null) {
            if (!NIPIsValid(value))
                return "^Wprowadź prawidłowy nr NIP (10 cyfr)";
        }
    };

    validate(form, constraints);

    form.addEventListener("submit", function (ev) {
        ev.preventDefault();
        handleFormSubmit(form);
    });

    var inputs = document.querySelectorAll("input, textarea, select")
    for (var i = 0; i < inputs.length; ++i) {
        inputs.item(i).addEventListener("change", function (e) {
            var errors = validate(form, constraints) || {};
            showErrorsForInput(this, errors[this.name])
        });
    }

    function objectifyForm(formArray) {

        var returnArray = {};
        for (var i = 0; i < formArray.length; i++) {
            returnArray[formArray[i]['name']] = formArray[i]['value'];
        }
        return returnArray;
    }

    function handleFormSubmit(form, input) {
        var errors = validate(form, constraints);
        showErrors(form, errors || {});
        if (!errors && $form.data('submitted') !== true) {
            $form.data('submitted', true);
            $('body').css('cursor', 'wait');
            $form.addClass('disabled');
            var contact = $('#contact').serializeArray();
            contact.push({name: 'rules', value: true});
            contact.push({name: 'rules_2', value: true});
            contact.push({name: 'utm_source', value: getUrlParameter('utm_source')});

            var dataSend = JSON.stringify(objectifyForm(contact));
            $.ajax({
                type: 'POST',
                url: 'https://mauto-master-api-webnp.vmlcloud.pl/api/v1/contact/',
                data: dataSend,
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                success: function (data, stats) {
                    $('#contact').text('').append($('#typ').toggleClass('d-block'));
                    $('body').css('cursor', 'auto');
                    $form.removeClass('disabled');
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $form.data('submitted', false);
                    $('body').css('cursor', 'auto');
                    $form.removeClass('disabled');
                }
            });
        }
    }

    function showErrors(form, errors) {
        $.each(form.querySelectorAll("input[name], select[name]"), function (key, input) {
            showErrorsForInput(input, errors && errors[input.name]);
        });
    }

    function showErrorsForInput(input, errors) {
        var formGroup = closestParent(input.parentNode, "text-field")
            , messages = formGroup.querySelector(".messages");
        resetFormGroup(formGroup);
        if (errors) {
            formGroup.classList.add("has-error");
            $.each(errors, function (key, error) {
                addError(messages, error);
            });
        } else {
            formGroup.classList.add("has-success");
        }
    }

    function closestParent(child, className) {
        if (!child || child == document) {
            return null;
        }
        if (child.classList.contains(className)) {
            return child;
        } else {
            return closestParent(child.parentNode, className);
        }
    }

    function resetFormGroup(formGroup) {
        formGroup.classList.remove("has-error");
        formGroup.classList.remove("has-success");
        $.each(formGroup.querySelectorAll(".messages.field-error"), function (key, el) {
            el.innerText = '';
        });
    }

    function addError(messages, error) {
        messages.classList.add("field-error");
        messages.innerText = error;
        if (error.length > 21 && $(window).width() > 768) {
            messages.innerText = error.substr(0, 21) + '...';
        }
        messages.title = error;
    }

    $('.show').on('click', function (e) {
        e.preventDefault();
        $(this).hide();
        $(this).parent().find('span').show();
    });

    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

    function NIPIsValid(nip) {
        var weights = [6, 5, 7, 2, 3, 4, 5, 6, 7];
        nip = nip.replace(/[\s-]/g, '');

        if (nip.length == 10 && parseInt(nip, 10) > 0) {
            var sum = 0;
            for (var i = 0; i < 9; i++) {
                sum += nip[i] * weights[i];
            }
            return (sum % 11) == nip[9];
        }
        return false;
    }

    $('.close').on('click', function () {
        $(modal).hide();
        $form.data('submitted', false);
        $form[0].reset();
        generateHash();
    });

    window.onclick = function (e) {
        if (e.target == modalBackground) {
            $(modal).hide();
            $form.data('submitted', false);
            $form[0].reset();
            generateHash();
        }
    }
});
